module Songs
  Versions = {
    beer_song: {
      location: 'on the wall',
      container: ->(number) { number == 1 ? 'bottle' : 'bottles' },
      beverage: 'beer',
      share: 'pass it around',
      supplier: 'store',
      action: 'and buy some more',
      item: ->(number) { number == 1 ? 'it' : 'one' },
      fully_stocked: 99  # 'borrowed' this variable name from KOTP solution.
    },

    moonshine_song: {
      location: 'on the still',
      container: ->(number) { number == 1 ? 'leather skin' : 'leather skins'},
      beverage: 'double strength moonshine',
      share: "don't share with anyone",
      supplier: 'backwoods',
      action: 'and get some more',
      item: ->(number) { number == 1 ? 'me' : 'one'},
      fully_stocked: 5
    }
  }
end

class BeerSong
  include Songs

  def self.recite(number, repeats, version = Versions[:beer_song])
    new(number, repeats, version).recite
  end

  private

  attr_reader :repeats, :version, :song, :number

  def initialize(number, repeats, version = Versions[:beer_song])
    @number = number
    @repeats = repeats
    @version = version
    @song = ''
  end

  def beverages(number)
    "#{number.zero? ? 'no more' : number} #{version[:container][number]} of #{version[:beverage]}"
  end

  def include_in_verse
    "#{beverages(number)} #{version[:location]}, #{beverages(number)}"
  end

  def general_verse
    <<~VERSE
      #{include_in_verse}.
      Take #{version[:item][number]} down and #{version[:share]}, #{beverages(@number -= 1)} #{version[:location]}.
    VERSE
  end

  def last_verse
    <<~LAST
      #{include_in_verse.capitalize}.
      Go to the #{version[:supplier]} #{version[:action]}, #{beverages(version[:fully_stocked])} #{version[:location]}.
    LAST
  end

  public

  def recite
    number.downto(number - repeats + 1) do
      return @song << last_verse unless number.positive?

      @song << general_verse << "\n"
    end
    song.chomp
  end
end

if $PROGRAM_NAME == __FILE__
  include Songs
  puts BeerSong.recite(2, 3, Versions[:moonshine_song])
end
