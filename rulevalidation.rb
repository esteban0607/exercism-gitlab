
class InequalityRules

  attr_accessor :rule
  attr_reader :randomized, :sides

  OPTIONS = {
    number_of_tests: 10,
    max_range: 100
  }.freeze

  RULES = {
    # submitted
    sum_of_sides_minus_max_gt_max: lambda do |sides|
      sides.sum - sides.max > sides.max
    end,
    # karlaalvarezlaboratoria
    permutations_sum_of_sides_gt_residual_side: lambda do |sides|
      sides.permutation(3).all? { |(a, b, c)| a + b > c }
    end,
    sum_of_lesser_sides_gt_max: lambda do |sides|
      sides.min(2).sum(0.0) > sides.max # kotp: swapped sort for min(2)
    end,
    # teepha (similar to Victor Goff)
    enumerable_all_sum_of_sides_minus_residual_side: lambda do |sides|
      sides.all? { |value| value < sides.sum - value }
    end,
    # andrewbaldwin44
    sum_of_lesser_sides_using_min_gt_max: lambda do |sides|
      sides.min(2).sum > sides.max
    end,
    # morporkk
    pop_max_lt_sum_of_lesser_sides: lambda do |sides|
      dim = sides.sort
      dim.pop < dim.sum
    end,
    permutations_verbose_as_gold_standard: lambda do |sides|
      (sides[0] + sides[1] > sides[2]) &&
        (sides[1] + sides[2] > sides[0]) &&
        (sides[0] + sides[2] > sides[1])
    end
  }.freeze

  def initialize
    @sides = sides
    @rule = rule
    @randomized = :randomized
    @report = ''
    @randomized = Array.new(OPTIONS[:number_of_tests]) do
      Array.new(3) { rand(OPTIONS[:max_range]) }
    end
  end

  def all_tests_except_gold_standard
    RULES.collect { |key, _| key }.reject do |key|
      key == :permutations_verbose_as_gold_standard
    end
  end

  def report
    text = ''
    @randomized.each do |sides|
      gold_standard = RULES[:permutations_verbose_as_gold_standard][sides]
      test_rule = RULES[@rule][sides]
      validated = test_rule == gold_standard
      if validated
        text = 'PASSED' << "\n"
      else
        text << validated.to_s.ljust(8) + sides.to_s.ljust(15) + gold_standard.to_s.ljust(10) << "\n"
      end
    end
    text
  end
end
