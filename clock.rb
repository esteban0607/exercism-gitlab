class Clock

  MINUTES = 60
  DAY = 24

  private

  def initialize(m = 0, h = 0, minute: m, hour: h)
    @minutes = ((hour * MINUTES) + minute).round
    to_h_m
  end

  def to_h_m
    h, m = minutes.divmod(MINUTES)
    @h_m = h % DAY, m
  end

  protected
  attr_reader :h_m, :minutes

  public

  def ==(other)
    h_m == other.h_m
  end

  def -(other)
    @minutes += -other.minutes
    self
  end

  def +(other)
    @minutes += other.minutes
    self
  end

  def to_s
    "%02d:%02d" % to_h_m
  end
end
