class ArmstrongNumbers
  def self.include?(integer)
    digits = integer.digits
    count = digits.count
    integer == digits.sum { |int| int**count }
  end
end
