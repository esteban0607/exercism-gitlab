require 'benchmark/ips'
require 'prime'

class PrimeFactors_with_Prime
  def self.of(natural)
    new.of(natural)
  end

  def of(natural)
    return [] if natural == 1

    Prime.each_with_object(factors = []) do |prime|
      while (natural % prime).zero?
        factors << prime
        natural /= prime
        return factors if natural == 1
      end
    end
  end
end

class PrimeFactors_without_Prime
  def self.of(natural)
    new.of(natural)
  end

  def of(natural)
    return [] if natural == 1

    (2..).each_with_object(factors = []) do |prime|
      while (natural % prime).zero?
        factors << prime
        natural /= prime
        return factors if natural == 1
      end
    end
  end
end


def benchmark(natural)
  Benchmark.ips do |x|
    x.report('With_Prime') { PrimeFactors_with_Prime.of(natural) }
    x.report('Without_Prime') { PrimeFactors_without_Prime.of(natural) }

    x.compare!
  end
end

if $PROGRAM_NAME == __FILE__
  benchmark(27)
end
