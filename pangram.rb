class Pangram

  ALPHABET = {
    English: %w|
     q w e r t y u i o p
      a s d f g h j k l
       z x c v b n m
    |,
      # (c) 2020, KOTP

    Hawaiian: %w|
      a e i o u h k l m n p w
    |
  }

  def self.pangram?(phrase)
    new(phrase).pangram?
  end

  def initialize(phrase, alphabet = ALPHABET[:English])
    @phrase = phrase.downcase
    @alphabet = alphabet
  end

  private

  attr_reader :alphabet, :phrase

  public

  def pangram?
    (alphabet.to_a - phrase.scan(/[a-z]/)).empty?
  end

end

if $PROGRAM_NAME == __FILE__
  phrase1 = 'the quick brown fox jumps over the lazy dog'
  puts Pangram.pangram?(phrase1)
  puts Pangram.new(phrase1).pangram?

  phrase2 = 'Never shall a hungry man go without his Leatherman'
  puts Pangram.pangram?(phrase2)
  puts Pangram.new(phrase2).pangram?

  phrase3 = 'Hiki iaʻu ke ʻai i ke aniani; ʻaʻole nō lā au e ʻeha.'
  puts Pangram.new(phrase3, Pangram::ALPHABET[:Hawaiian]).pangram?
end
