module ResistorConstants
  # rubocop:disable MutableConstant

  BANDS = %w[black brown red orange yellow green blue violet grey white]
  MESSAGES = {
    color_error: 'Valid colors are: %s ' % BANDS.join(', ')
  }
  # rubocop:enable MutableConstant
end

module ResistorExceptions
=begin rdoc
1. Your Resistor class should require this module in order to access Resistor specific custom Error messages.
2. This ResistorExceptions module requires the ResistorConstants module.
=end

  include ResistorConstants

  class BandColorError < ArgumentError

    def initialize(message = ResistorConstants::MESSAGES[:color_error])
      super
    end
  end
end

class ResistorColorTrio
  include ResistorExceptions
  attr_accessor :label

  def initialize(colors)
    raise BandColorError unless (colors - BANDS).empty?

    digit1, digit2, power = colors.map { |color| BANDS.index(color) }
    resistance = ((digit1 * 10) + digit2) * 10**power
    unit = resistance < 1000 ? "#{resistance} ohms" : "#{resistance / 1000} kiloohms"
    @label = "Resistor value: #{unit}"
  end
end
