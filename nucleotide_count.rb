module Nucleotide

  NUCLEOTIDES = %w(A C G T)

  def self.from_dna(strand)
    @strand = valid_strand?(strand.scan(/\D/))
    self
  end

  def self.count(nucleotide)
    histogram.fetch(nucleotide)
  end

  def self.histogram
    default_hash = { 'A' => 0, 'T' => 0, 'C' => 0, 'G' => 0 }
    @strand.each_with_object(default_hash) { |character, hash| hash[character] += 1 }
  end

  def self.valid_strand?(strand)
    raise ArgumentError, "Only #{NUCLEOTIDES} are valid." unless (strand - NUCLEOTIDES).empty?

    strand
  end
end
