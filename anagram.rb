class Anagram

  attr_reader :sorted_chars, :anagrams

  def initialize(anagrams)
    @anagrams = anagrams.downcase
    @sorted_chars = @anagrams.chars.sort
  end

  def match(words)
    words.select do |word|
      downcase_word = word.downcase
      downcase_word.chars.sort == sorted_chars &&
        downcase_word != reference
    end
  end
end
