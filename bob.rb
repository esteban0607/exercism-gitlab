class Bob
  Question = ->(phrase) { phrase.strip.end_with? '?' }
  Forceful = ->(phrase) { /[A-Z]/.match(phrase) && phrase.upcase == phrase }
  Silence = ->(phrase) { phrase.strip.empty? }
  Exclamation = ->(phrase) { phrase.end_with? '!' }

  Response = {
    question: 'Sure.',
    forceful: "Calm down, I know what I'm doing!",
    silence: "Fine. Be that way!",
    exclamation: "Whoa, chill out!",
    whatever: 'Whatever.'
  }

  def self.hey(phrase)
    @phrase = phrase
    Response[response_style]
  end

  def self.response_style
    case @phrase
    when Question
      Forceful[@phrase] ? :forceful : :question
    when Silence
      :silence
    when Exclamation && Forceful || Forceful
      :exclamation
    else
      :whatever
    end
  end
end
