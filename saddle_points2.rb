class Matrix

  attr_reader :rows, :columns

  def initialize(string)
    @rows = string.lines.map { |line| line.chomp.split.map(&:to_i) }
    @columns = rows.transpose
  end

  def saddle_points
    rows
      .map(&:max)
      .each_with_index
      .each_with_object(saddle = [[]]) do |(row, row_index)|
        columns.map(&:min).each_with_index do |col, col_index|
          saddle << [row_index, col_index] if row == col
      end
    end
    .reject!(&:empty?)
  end
end
