class Acronym
  def self.abbreviate(phrase)
    phrase.upcase.scan(/\b[A-Z]/).join
  end
end

if $PROGRAM_NAME == __FILE__

puts expected = 'MEAAQNSB'
input = 'Matz es agradable, ¡así que nosotros somos buenos!'
output = Acronym.abbreviate(input)
puts output.inspect
puts expected == output

puts expected2 = 'ROR'
input2 = 'Ruby on Rails'
output2 = Acronym.abbreviate(input2)
puts output2.inspect
puts expected2 == output2

end
