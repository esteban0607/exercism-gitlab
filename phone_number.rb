class PhoneNumber
  Regexp = {
    NANP:  /[2-9]\d\d[2-9]\d{6}/,
    NOT_DIGITS: /\D+/
  }

  NumberPlan = {
    Country: '+1',
    Area: 0..2,
    Exchange: 3..6,
    Line: 6..9
  }

  attr_reader :country,
              :area,
              :exchange,
              :line
  attr_reader :phone_number

  def self.clean(number)
    new(number).phone_number
  end

  private

  def initialize(number)
    @phone_number = number.dup
    @phone_number.gsub!(Regexp[:NOT_DIGITS], '')
    @phone_number = '' unless valid?(phone_number)
    clean_number
  end

  def valid?(phone_number)
    (10..11).include? phone_number.length
  end

  def  clean_number
    if phone_number.length == 11
      @phone_number = phone_number.start_with?('1') ? phone_number[1..-1] : ''
    end
    set_codes
    @phone_number = phone_number =~ Regexp[:NANP] ? phone_number : nil
  end

  def set_codes
    @country = NumberPlan[:Country]
    @area, @exchange, @line =
      [NumberPlan[:Area], NumberPlan[:Exchange], NumberPlan[:Line]].map do |range|
        phone_number[range]
      end
  end
end
# Examples
if $PROGRAM_NAME == __FILE__
  phone_number = PhoneNumber.new("12234567890")
  puts phone_number.phone_number
  puts phone_number.country
  puts phone_number.area
  puts phone_number.exchange
  puts phone_number.line
  puts PhoneNumber.clean("223 456   7890   ")
end