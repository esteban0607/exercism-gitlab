class Raindrops

  DROPS = {
    3 => 'Pling',
    5 => 'Plang',
    7 => 'Plong'
  }

  def self.convert(number)
    drops = DROPS.select { |key| (number % key).zero? }
    drops.any? ? drops.values.join : number.to_s
  end
end