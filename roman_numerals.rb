module Roman

  UNITS = [
    %w[   1: I II III IV V VI VII VIII IX],
    %w[  10: X XX XXX XL L LX LXX LXXX XC],
    %w[ 100: C CC CCC CD D DC DCC DCCC CM],
    %w[1000: M MM MMM]
  ]

  refine Integer do
    def to_roman
      digits.each_index.each_with_object(numeral = '') do |decimal|
        digit = digits[decimal]
        symbol = UNITS[decimal][digit]
        numeral.prepend(symbol) unless digit.zero?
      end
    end
  end
end

class Integer
  include Roman
end

if $PROGRAM_NAME == __FILE__
  using Roman
  puts 233.to_roman
end
