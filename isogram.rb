class Isogram

  REGEX = {
    alphanumeric: /\w/
  }

  def self.isogram?(phrase)
    new(phrase).isogram?
  end

  private

  def initialize(phrase)
    @alphanumeric = phrase.downcase.scan(REGEX[:alphanumeric])
    @singles = alphanumeric.uniq
  end

  attr_reader  :alphanumeric,
               :singles

  public

  def isogram?
    alphanumeric == singles
  end

end