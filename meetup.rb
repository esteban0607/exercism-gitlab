require 'date'

class Meetup

  Descriptor = {
    first:  ->(matches) { matches.first },
    second: ->(matches) { matches[1] },
    third:  ->(matches) { matches[2] },
    fourth: ->(matches) { matches[3] },
    fifth:  ->(matches) { matches[4] },
    last:   ->(matches) { matches.last },
    teenth: ->(matches) { (matches.find { |day| (12..18).include? day }) }
  }

  private

  attr_reader :date, :month, :year, :calendar

  def initialize(month, year)
    @date = Date.new(year, month, 1)
    @month = month
    @year = year
    @calendar = calendar_months
  end

  def days_in_month
    Date.new(year, month, -1).day
  end

  def calendar_months
    offset = date.cwday
    week = 0
    (offset...days_in_month + offset).reduce([]) do |accumulator, day|
      week += 1 if (day % 7).zero?
      accumulator << day - (week * 7)
    end
  end

  def select_dates(weekday)
    calendar.each_index.select do |index|
      calendar[index] == Date::DAYNAMES.index(weekday)
    end
  end

  public

  def day(weekday, descriptor)
    selected_date = Descriptor[descriptor][select_dates(weekday.to_s.capitalize)].next
    Date.new(year, month, selected_date)
  end
end

