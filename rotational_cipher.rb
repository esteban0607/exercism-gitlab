class RotationalCipher

  attr_reader :lower, :upper, :alphabet, :phrase, :distance

  def initialize(phrase, distance)
    @phrase = phrase.chars
    @distance = distance
    @lower = ('a'..'z').to_a
    @upper = ('A'..'Z').to_a
    @alphabet = @lower + @upper
  end

  def self.rotate(phrase, distance)
    new(phrase, distance).to_s
  end

  def rotate
    zipped = alphabet.zip(lower.rotate(distance) + upper.rotate(distance)).to_h
    phrase.map { |n| zipped[n] || n }.join
  end

  def to_s
    rotate
  end
end

