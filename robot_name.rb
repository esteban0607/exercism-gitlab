class Robot
  Names = ('AA000'..'ZZ999').to_a.shuffle
  @@names = Names.dup

  def self.forget
    @@names = Names.dup
  end

  private

  def initialize
    new_name
  end

  def new_name
    @name = @@names.pop
  end

  public

  attr_reader :name

  def reset
    new_name
  end
end
