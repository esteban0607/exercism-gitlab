module Accumulatable
  def accumulate
    map { |element| yield(element) }
  end
end

Array.include Accumulatable