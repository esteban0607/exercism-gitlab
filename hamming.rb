module GeneExceptions
  class StrandLengthError < ArgumentError
    def initialize(message = 'Strand lengths must be identical')
      super
    end
  end
end

class Hamming
  include GeneExceptions
  attr_reader :distance

  def self.compute(strand1, strand2)
    new(strand1, strand2).distance
  end

  def initialize(strand1, strand2)
    raise StrandLengthError unless strand1.length == strand2.length

    nucleotides = strand1.chars.zip(strand2.chars)
    @distance = nucleotides.count { |n1, n2| n1 != n2 }
  end

end
