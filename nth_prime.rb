class ZerothPrimeError < ArgumentError

  def initialize(message = "There is no Zero'th prime number.")
    super
  end
end

class Prime

  def self.nth(nth_prime)
    new(nth_prime).nth
  end

  private

  attr_reader :nth_prime, :prime, :primes

  def initialize(nth_prime)
    @nth_prime = nth_prime
    @prime = 1
    @primes = 2
  end

  public

  def nth
    return nth_prime.next if [1, 2].include?(nth_prime)
    raise ZerothPrimeError if nth_prime.zero?

    3.step(by: 2) do |i|
      if i.prime?
        @prime = i
        return prime if primes == nth_prime

        @primes = primes.next
      end
    end
  end
end

class Integer
  def prime?
    (2..Math.sqrt(self)).none? { |i| (self % i).zero? }
  end
end
