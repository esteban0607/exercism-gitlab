require_relative 'rulevalidation'
class Triangle
  attr_reader :sides
  attr_accessor :inequality_rule

  def initialize(sides, inequality_rule = InequalityRules::RULES[:sum_of_sides_minus_max_gt_max][sides])
    @inequality_rule = inequality_rule
    @sides = sides
  end

  def equilateral?
    triangle? && sides.uniq.count == 1
  end

  def isosceles?
    triangle? && sides.uniq.count <= 2
  end

  def scalene?
    triangle? && sides.uniq.count == 3
  end

  def triangle?
    @sides.all?(&:positive?) && @inequality_rule
  end
end


if __FILE__ == $PROGRAM_NAME
  sides = [6, 8, 3]
  triangle = Triangle.new(sides)
  compare = InequalityRules.new
  compare.all_tests_except_gold_standard.each do |rule|
    compare.rule = rule
    triangle.inequality_rule = rule
    datum = {
        rule: rule,
        sides: sides.join(', '),
        equilateral: triangle.equilateral?,
        scalene: triangle.scalene?,
        isosceles: triangle.isosceles?,
        results: compare.report
    }
    report = <<~REPORT % datum
      =======================
      TESTING: %<rule>s
      =======================
      The triangle with sides as %<sides>s:
      Equilateral? %<equilateral>s
      Scalene? %<scalene>s
      Isosceles? %<isosceles>s
      Inequality?: %<results>s
    REPORT
    puts report
  end
end

# - karlaalvarezlaboratoria https://exercism.io/tracks/ruby/exercises/triangle/solutions/c4c6184b40f344488198e1d21215e5f7
# - andrewbaldwin44 https://exercism.io/tracks/ruby/exercises/triangle/solutions/ec79f1aa23f7498db4aeb724132f00c7
# - kolkopattern https://exercism.io/tracks/ruby/exercises/triangle/solutions/894a1dfb8dfa414591efbf9f63165d9f
# - morporkk https://exercism.io/tracks/ruby/exercises/triangle/solutions/24d8b83cae75471aaa199e9f6e822cbc
# - teepha https://exercism.io/tracks/ruby/exercises/triangle/solutions/8722ff8acb524df3aeadcaa8c1a1cfc3