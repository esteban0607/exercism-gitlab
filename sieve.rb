class Sieve
  def initialize(limit)
    @limit = limit
    @list = (2..limit).to_a
  end

  private

  attr_reader :limit, :list

  public

  def primes
    multiples = list.map do |numerator|
      (numerator..limit).map { |integer| integer * numerator }
    end.flatten
    list - multiples
  end
end
