class PrimeFactors
  def self.of(natural)
    (2..).each_with_object([]) do |prime, factors|
      return factors if natural == 1

      while (natural % prime).zero?
        factors << prime
        natural /= prime
      end
    end
  end
end
