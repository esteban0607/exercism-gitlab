module Categorize
  def keep
    select { |element| yield(element) }
  end

  def discard
    select { |element| !yield(element) }
  end
end

Array.include Categorize
