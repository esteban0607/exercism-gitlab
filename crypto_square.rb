class Crypto

  private

  attr_reader :plaintext, :length

  def initialize(plaintext)
    @plaintext = plaintext.downcase.delete('^a-z0-9')
    @length = @plaintext.length
  end

  def rectangle
    column = Math.sqrt(length).ceil
    row = (length / column.to_f).ceil
    [row, column]
  end

  def slices
    row, column = rectangle
    pad = (row * column) - length
    (plaintext << ' ' * pad).chars.each_slice(column).to_a
  end

  public

  def ciphertext
    return '' if length.zero?

    slices.transpose.map(&:join).join(' ')
  end
end

