class Tournament
  Header = '%-31s| MP |  W |  D |  L |  P' % 'Team'
  Reciprocal = {'win' => 'loss', 'draw' => 'draw', 'loss' => 'win'}

  def self.tally(input)
    new(input).tally
  end

  private

  attr_accessor :data, :matches, :teams, :stats

  def initialize(input)
    @data = input.lines.map(&:chomp).map { |line| line.split(';') }
    @matches = Hash.new {[]}
    @teams = {}
    @stats = {}
    add_matches
  end

  def add_matches
    data.map do |team_A, team_B, score|
      matches[team_A] += [score]
      matches[team_B] += [Reciprocal[score]]
    end
  end

  def create_statistics
    teams.each do |name, scores|
      %w[win draw loss].each { |state| scores[state] = 0 unless scores[state] }
      win = scores['win']
      draw = scores['draw']
      loss = scores['loss']
      matches = win + draw + loss
      points = (win * 3) + draw
      stats.merge!(name => { name: name, matches: matches, win: win, draw: draw, loss: loss, points: points })
    end
  end

  def report
    Header + "\n" +
      stats.values
          .sort_by { |scores| [-scores[:points], scores[:name]] }
          .map do |sorted|
            "%-30s | %2d | %2d | %2d | %2d | %2d\n" %
              [
                sorted[:name],
                sorted[:matches],
                sorted[:win],
                sorted[:draw],
                sorted[:loss],
                sorted[:points]
              ]
    end.join
  end

  public

  def tally
    matches.each { |name, scores| teams.merge!({ name => scores.tally }) }
    create_statistics
    to_s
  end

  def to_s
    teams.include?(nil) ? Header + "\n" : report
  end
end