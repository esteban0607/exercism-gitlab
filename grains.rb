module Grains
  BOARD = (1..64)

  class LocationError < ArgumentError
    def initialize(message = 'The number of squares are outside of the range')
      super
    end
  end


  def self.square(location)
    raise LocationError unless BOARD.include? location

    2**(location - 1)
  end

  def self.total
    2**BOARD.max - 1
  end
end