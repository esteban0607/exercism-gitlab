class Squares

  def initialize(series)
    @series = (1..series)
  end

  def sum_of_squares
    @series.sum { |value| value**2 }
  end

  def square_of_sum
    @series.sum**2
  end

  def difference
    square_of_sum - sum_of_squares
  end
end

