# Elegant way to do transpose From Exercism

class Transpose
  def self.transpose(input, output = [])
    rows = input.lines.map(&:rstrip).map(&:chars)

    rows.each_with_index do |row, i|
      row.each_index do |j|
        output[j] ||= []
        output[j][i] = rows[i][j]
        (0...i).each { |k| output[j][k] ||= ' ' }
      end
    end

    output.map(&:join).join("\n")
  end
end