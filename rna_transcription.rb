class Complement

  def self.of_dna(strand)
    strand.upcase.tr('GCTA', 'CGAU')
  end
end