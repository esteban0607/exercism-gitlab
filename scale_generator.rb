class Scale

  Notes = {
    sharps: %w[A A# B C C# D D# E F F# G G# A A# B C C# D D# E F F# G G#],
     flats: %w[A Bb B C Db D Eb E F Gb G Ab A Bb B C Db D Eb E F Gb G Ab]
  }

  Accents = {
    sharps: %w[C a G D A E B F# e b f# c# g# d#],
     flats: %w[F Bb Eb Ab Db Gb d g c f bb eb]
  }

  Chromatic ='mmmmmmmmmmmm'

  private

  attr_reader :given_name, :interval, :notes

  def initialize(given_name, key, interval = Chromatic)
    @given_name = given_name
    @name = given_name.dup.upcase << " " << key.to_s
    @interval = interval
    @notes = Notes[accents]

  end

  def accents
    Accents.each do |accent, notes|
      return accent if notes.include? given_name
    end
  end

  def intervals
    interval.tr('AMm', '321').chars.map(&:to_i)
  end

  public

  attr_reader :name

  def pitches
    offset = notes.find_index(given_name.capitalize)
    intervals.each_with_index.each_with_object(pitch = []) do |(step)|
      pitch << notes[offset]
      offset += step
    end
  end
end

