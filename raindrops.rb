class Raindrops

  RULES = {
    3 => 'Pling',
    5 => 'Plang',
    7 => 'Plong'
  }

  def self.convert(number)
    new(number).to_s
  end

  def initialize(number, rules = RULES)
    @number = number
    @rules = rules
  end

  private

  attr_reader :number, :rules

  def transform
    translation = translate
    translation.empty? ? number : translation
  end

  def translate
    rules.each_with_object(translation = '') do |(factor, drops)|
      translation << drops if (number % factor).zero?
    end
  end

  public

  def to_s
    transform.to_s
  end
end

if $PROGRAM_NAME == __FILE__
  fizzbuzz = { 3 => 'Fizz', 5 => 'Buzz' }
  -15.upto(15) do |integer|
    puts Raindrops.new(integer, fizzbuzz)
  end
end