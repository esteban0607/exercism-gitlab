class Phrase

  attr_reader :words

  def initialize(phrase)
    @words = phrase
      .gsub(/'([\S]+)'/, '\1')
      .downcase
      .scan(/[\w']+/)
  end

  def word_count
    words.each_with_object(Hash.new(0)) { |word, hash| hash[word] += 1 }
  end
end