class Luhn

  def self.valid?(characters)
    new(characters).valid?
  end

  private

  attr_reader :characters

  def initialize(characters)
    @characters = characters.tr(' ', '')
    valid?
  end

  def checksum
    digits = characters
             .to_i
             .digits
             .map.with_index { |digit, index| index.odd? ? digit * 2 : digit }
    (digits.map { |i| i > 9 ? i - 9 : i }.sum % 10).zero?
  end

  public

  def valid?
    checksum unless characters =~ /\D/ || (0..1).include?(characters.size)
  end
end