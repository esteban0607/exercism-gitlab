class Matrix
  attr_accessor :rows, :columns
  def initialize(matrix)
    @rows = matrix.lines.map { |str| str.split.map(&:to_i) }
    @columns = rows.transpose
  end
end