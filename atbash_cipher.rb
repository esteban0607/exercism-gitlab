class Atbash
  Alphabet = 'abcdefghijklmnopqrstuvwxyz'
  Chunk = /.{1,5}/
  Noise = '^a-zA-Z0-9'

  def self.encode(plaintext)
    new(plaintext).encode
  end

  def self.decode(ciphertext)
    new(ciphertext).decode
  end

  private

  attr_reader :phrase

  def initialize(phrase)
    @phrase = phrase.tr(Noise, '').downcase.tr(Alphabet, Alphabet.reverse)
  end

  public

  def encode
    phrase.scan(Chunk).join(' ')
  end

  def decode
    phrase
  end
end

