class Matrix

  attr_reader :rows, :columns

  def initialize(string)
    @rows = string.lines.map { |line| line.split.map(&:to_i) }
    @columns = rows.transpose
  end

  def saddle_points
    rows
      .map(&:max)
      .to_enum
      .with_index
      .with_object(saddle = [[]]) do |(row,row_index)|
        columns
          .map(&:min)
          .each_with_index do |column, column_index|
        saddle << [row_index, column_index] if row == column
      end
    end.reject!(&:empty?)
  end
end
