class HighScores
  attr_reader :scores

  def initialize(scores)
    @scores = scores.dup
  end

  def latest
    scores.last
  end

  def personal_best
    personal_top_three.max
  end

  def personal_top_three
    scores.max(3)
  end

  def latest_is_personal_best?
    latest == personal_best
  end

end
