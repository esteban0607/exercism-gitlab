class Series

  private

  attr_reader :digits

  def initialize(digits)
    @digits = digits.chars
  end

  public

  def slices(size)
    raise InvalidSliceError unless size <= digits.length

    digits.each_cons(size).map(&:join)
  end
end

class InvalidSliceError < ArgumentError
  def initialize(message = 'Slice size exceeds available data size.')
    super
  end
end
