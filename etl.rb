class ETL
  def self.transform(data)
    data.each_with_object(transformed = {}) do |(key, values)|
      values.each { |value| transformed.store(value.downcase, key) }
    end
  end
end