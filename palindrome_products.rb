class Palindromes

  def initialize(min_factor: 1, max_factor:)
    @range = (min_factor..max_factor).to_a
  end

  private

  attr_reader :range,
              :palindromes

  Palindrome = Struct.new(:value, :factors)

  def palindrome?(value)
    value.digits == value.digits.reverse
  end

  public

  def generate
    factors = range.repeated_combination(2)
    factors.each_with_object(@palindromes = {}) do |(f1, f2)|
      product = f1 * f2
      (palindromes[product] ||= []) << [f1, f2] if palindrome?(product)
    end
  end

  def largest
    Palindrome.new(*palindromes.max)
  end

  def smallest
    Palindrome.new(*palindromes.min)
  end

end