
class Triplet

  attr_reader :triplet

  def initialize(a, b, c)
    @triplet = [a, b, c]
  end

  def self.where(max_factor:, min_factor: 1, sum: nil)
    (min_factor..max_factor)
      .to_a.combination(3)
      .map { |triangle_sides| new(*triangle_sides) }
      .select do |triplet|
      triplet.pythagorean? && (sum.nil? || triplet.sum == sum)
    end
  end

  def sum
    triplet.sum
  end

  def product
    triplet.reduce(:*)
  end

  def pythagorean?
    triplet.min(2).sum { |value| value**2 } == triplet.max**2
  end
end
